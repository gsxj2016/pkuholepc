package io.github.gsxj2014.pkuholetest;

import io.github.gsxj2014.pkuhole.Comment;
import io.github.gsxj2014.pkuhole.PkuHole;
import io.github.gsxj2014.pkuhole.PkuHoleBoomException;
import io.github.gsxj2014.pkuhole.Topic;
import org.junit.Test;

import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HoleTest {

    private PkuHole hole = new PkuHole();
    private final Logger logger = Logger.getGlobal();

    public HoleTest() {
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(Level.INFO);
        logger.addHandler(consoleHandler);
    }

    @Test
    public void getListTest() throws PkuHoleBoomException {
        List<Topic> topics = hole.getTopics(1);
        logger.log(Level.INFO, "topics: " + topics.toString());
    }

    @Test
    public void getCommentTest() throws PkuHoleBoomException {
        List<Comment> comments = hole.getComments(335830);
        logger.log(Level.INFO, "comments: " + comments.toString());
    }

}
