package io.github.gsxj2014.pkuholetest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.github.gsxj2014.pkuhole.Comment;
import io.github.gsxj2014.pkuhole.Topic;
import io.github.gsxj2014.pkuhole.TopicType;
import io.github.gsxj2014.pkuhole.TopicTypeDeserializer;
import org.junit.Assert;
import org.junit.Test;

public class DeserializationTest {

    @Test
    public void testTopic1() {
        //language=JSON
        String jsonExample = "{\n" +
                "  \"pid\":\"300000\",\n" +
                "  \"text\":\"hello\\n\\u5317\\u4eac\",\n" +
                "  \"type\":\"image\",\n" +
                "  \"timestamp\":\"100000000000\",\n" +
                "  \"reply\":\"233\",\n" +
                "  \"likenum\":\"23\",\n" +
                "  \"extra\":\"23333\",\n" +
                "  \"url\":\"testurl\"\n" +
                "}";
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(TopicType.class, new TopicTypeDeserializer())
                .create();
        Topic topic = gson.fromJson(jsonExample, Topic.class);
        Assert.assertNotNull(topic);
        Assert.assertEquals(300000, topic.id);
        Assert.assertEquals("hello\n\u5317\u4eac", topic.text);
        Assert.assertEquals(TopicType.IMAGE, topic.type);
        Assert.assertEquals(100000000000L, topic.time);
        Assert.assertEquals(233, topic.reply);
        Assert.assertEquals(23, topic.like);
        Assert.assertEquals(23333, topic.extra);
        Assert.assertEquals("testurl", topic.url);
    }

    @Test
    public void testTopic2() {
        //language=JSON
        String jsonExample = "{\n" +
                "  \"pid\":\"300000\",\n" +
                "  \"text\":\"hello2\",\n" +
                "  \"type\":\"text\",\n" +
                "  \"timestamp\":\"110000000000\",\n" +
                "  \"reply\":\"23\",\n" +
                "  \"likenum\":\"2\",\n" +
                "  \"extra\":\"0\",\n" +
                "  \"url\":\"\"\n" +
                "}";
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(TopicType.class, new TopicTypeDeserializer())
                .create();
        Topic topic = gson.fromJson(jsonExample, Topic.class);
        Assert.assertNotNull(topic);
        Assert.assertEquals(300000, topic.id);
        Assert.assertEquals("hello2", topic.text);
        Assert.assertEquals(TopicType.TEXT, topic.type);
        Assert.assertEquals(110000000000L, topic.time);
        Assert.assertEquals(23, topic.reply);
        Assert.assertEquals(2, topic.like);
        Assert.assertEquals(0, topic.extra);
        Assert.assertEquals("", topic.url);
    }

    @Test
    public void testTopicUnsupportedType() {
        //language=JSON
        String jsonExample = "{\n" +
                "  \"pid\":\"0\",\n" +
                "  \"text\":\"hello3\",\n" +
                "  \"type\":\"eeeee\",\n" +
                "  \"timestamp\":\"0\",\n" +
                "  \"reply\":\"0\",\n" +
                "  \"likenum\":\"0\",\n" +
                "  \"extra\":\"0\",\n" +
                "  \"url\":\"0\"\n" +
                "}";
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(TopicType.class, new TopicTypeDeserializer())
                .create();
        Topic topic = gson.fromJson(jsonExample, Topic.class);
        Assert.assertNotNull(topic);
        Assert.assertEquals(0, topic.id);
        Assert.assertEquals("hello3", topic.text);
        Assert.assertEquals(TopicType.TEXT, topic.type);
        Assert.assertEquals(0, topic.time);
        Assert.assertEquals(0, topic.reply);
        Assert.assertEquals(0, topic.like);
        Assert.assertEquals(0, topic.extra);
        Assert.assertEquals("0", topic.url);
    }

    @Test
    public void testComment1() {
        //language=JSON
        String jsonExample = "{\n" +
                "  \"cid\":\"123\",\n" +
                "  \"pid\":\"321\",\n" +
                "  \"text\":\"[Alice] \\u56ed\\u6e38\\u4f1a\",\n" +
                "  \"timestamp\":\"1000000001\",\n" +
                "  \"hidden\":\"0\",\n" +
                "  \"anonymous\":\"1\",\n" +
                "  \"islz\":0,\n" +
                "  \"name\":\"Alice\"\n" +
                "}";
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(TopicType.class, new TopicTypeDeserializer())
                .create();
        Comment comment = gson.fromJson(jsonExample, Comment.class);
        Assert.assertNotNull(comment);
        Assert.assertEquals(123, comment.cid);
        Assert.assertEquals(321, comment.pid);
        Assert.assertEquals("[Alice] \u56ed\u6e38\u4f1a", comment.text);
        Assert.assertEquals(1000000001, comment.time);
        Assert.assertEquals("Alice", comment.name);
    }
}
