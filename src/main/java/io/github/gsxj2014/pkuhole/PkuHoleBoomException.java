package io.github.gsxj2014.pkuhole;

public class PkuHoleBoomException extends Exception {
    PkuHoleBoomException(String message) {
        super(message);
    }
}
