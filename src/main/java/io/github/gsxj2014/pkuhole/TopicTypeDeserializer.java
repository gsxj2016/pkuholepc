package io.github.gsxj2014.pkuhole;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class TopicTypeDeserializer implements JsonDeserializer<TopicType> {

    @Override
    public TopicType deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        String typeStr = json.getAsString();
        return "image".equals(typeStr) ? TopicType.IMAGE : TopicType.TEXT;
    }
}
