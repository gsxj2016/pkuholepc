package io.github.gsxj2014.pkuhole;

import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.util.Date;

public class Topic {
    @SerializedName("pid")
    public long id;

    public TopicType type;

    public String text;

    @SerializedName("timestamp")
    public long time;

    public int reply;

    @SerializedName("likenum")
    public int like;

    public long extra;

    public String url;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder()
                .append("#").append(id)
                .append("|").append(text)
                .append("|").append(time)
                .append("|").append(reply)
                .append("|").append(like);
        if (type == TopicType.IMAGE) {
            sb.append("|").append(extra).append("|").append(url);
        }
        return sb.toString();
    }

    public String toFormattedString() {
        Date date = new Date(time * 1000L);
        String dateText = DateFormat.getDateInstance().format(date);
        return String.format("#%d-----%s (%d|%d) %s\n%s\n",
                id, dateText, reply, like,
                type == TopicType.IMAGE ? "[Pic]" : "",
                text);
    }
}
