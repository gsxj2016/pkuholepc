package io.github.gsxj2014.pkuhole;

import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.util.Date;

public class Comment {

    public long cid;

    public long pid;

    public String text;

    @SerializedName("timestamp")
    public long time;

    public String name;

    @Override
    public String toString() {
        return String.format("%s|%d|%s|%d|%s", String.valueOf(cid), pid, text, time, name);
    }

    public String toFormattedString() {
        Date date = new Date(time * 1000L);
        String dateText = DateFormat.getDateInstance().format(date);
        return String.format("#%d-----%s\n%s\n", cid, dateText, text);
    }
}
