package io.github.gsxj2014.pkuhole;

import com.google.gson.*;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import javax.imageio.ImageIO;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PkuHole {

    private static final String PKU_HOLE_API_URL = "http://pkuhelper.pku.edu.cn/services/pkuhole/api.php";
    private static final String PKU_HOLE_IMAGE_PATH = "http://pkuhelper.pku.edu.cn/services/pkuhole/images/";
    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.8143.0.2357.8143.0.2357.8143.0.2357.81 Safari/537.36";

    private JsonElement api(List<NameValuePair> args) throws PkuHoleBoomException {
        try {
            URL url = new URIBuilder(PKU_HOLE_API_URL)
                    .addParameters(args)
                    .build()
                    .toURL();

            try {
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setRequestProperty("User-token", "guest");
                conn.setRequestProperty("User-Agent", USER_AGENT);
                conn.setRequestMethod("GET");

                int status = conn.getResponseCode();
                if (status == 200) {
                    try (InputStreamReader response = new InputStreamReader(conn.getInputStream());
                         BufferedReader reader = new BufferedReader(response)) {
                        String line;
                        StringBuilder sb = new StringBuilder();
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }

                        JsonParser parser = new JsonParser();
                        return parser.parse(sb.toString());
                    }
                } else {
                    throw new PkuHoleBoomException(String.format("(%d) %s", status, conn.getResponseMessage()));
                }

            } catch (IOException e) {
                throw new PkuHoleBoomException("Network fail| " + e.getMessage());
            }

        } catch (URISyntaxException | MalformedURLException e) {
            throw new PkuHoleBoomException("Bad URL| " + e.getMessage());
        }
    }

    private <T> T parseResult(JsonElement element, Class<T> classOfT) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(TopicType.class, new TopicTypeDeserializer())
                .create();
        return gson.fromJson(element, classOfT);
    }

    private static NameValuePair pair(String name, String value) {
        return new BasicNameValuePair(name, value);
    }

    public List<Topic> getTopics(int page) throws PkuHoleBoomException {
        List<NameValuePair> nvp = Arrays.asList(
                pair("action", "getlist"),
                pair("p", Integer.toString(page)),
                pair("PKUHelperAPI", "3.0")
        );

        try {
            JsonElement json = api(nvp);
            return Arrays.asList(parseResult(json.getAsJsonObject().get("data"), Topic[].class));
        } catch (JsonParseException e) {
            throw new PkuHoleBoomException("Json Error| " + e.getMessage());
        }
    }

    public List<Topic> searchTopics(String keywords) throws PkuHoleBoomException {
        List<NameValuePair> nvp = Arrays.asList(
                pair("action", "search"),
                pair("keywords", keywords),
                pair("pagesize", "100"),
                pair("PKUHelperAPI", "3.0")
        );

        try {
            JsonElement json = api(nvp);
            return Arrays.asList(parseResult(json.getAsJsonObject().get("data"), Topic[].class));
        } catch (JsonParseException e) {
            throw new PkuHoleBoomException("Json Error| " + e.getMessage());
        }
    }

    public List<Topic> getOneTopic(String pid) throws PkuHoleBoomException {
        List<NameValuePair> nvp = Arrays.asList(
                pair("action", "getone"),
                pair("pid", pid),
                pair("PKUHelperAPI", "3.0")
        );

        try {
            JsonElement json = api(nvp);
            if (json.getAsJsonObject().has("data")) {
                return Collections.singletonList(parseResult(json.getAsJsonObject().get("data"), Topic.class));
            } else {
                return Collections.emptyList();
            }
        } catch (JsonParseException e) {
            throw new PkuHoleBoomException("Json Error| " + e.getMessage());
        }
    }

    public List<Comment> getComments(long pid) throws PkuHoleBoomException {
        List<NameValuePair> nvp = Arrays.asList(
                pair("action", "getcomment"),
                pair("pid", Long.toString(pid)),
                pair("PKUHelperAPI", "3.0")
        );

        try {
            JsonElement json = api(nvp);
            return Arrays.asList(parseResult(json.getAsJsonObject().get("data"), Comment[].class));
        } catch (JsonParseException e) {
            throw new PkuHoleBoomException("Json Error| " + e.getMessage());
        }
    }

    public Image getImage(Topic topic) throws PkuHoleBoomException {
        if (topic.type != TopicType.IMAGE) {
            throw new PkuHoleBoomException("Image Error| Bad topic type");
        }

        try {
            URL url = new URL(PKU_HOLE_IMAGE_PATH + topic.url);
            return ImageIO.read(url);
        } catch (IOException e) {
            throw new PkuHoleBoomException("Image Error| " + e.getMessage());
        }
    }
}
