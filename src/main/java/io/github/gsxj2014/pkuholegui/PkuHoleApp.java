package io.github.gsxj2014.pkuholegui;

import io.github.gsxj2014.pkuhole.PkuHole;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class PkuHoleApp {

    static final PkuHole PKU_HOLE = new PkuHole();


    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | UnsupportedLookAndFeelException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }

        SwingUtilities.invokeLater(MainWindow::new);
    }

}
