package io.github.gsxj2014.pkuholegui;

import io.github.gsxj2014.pkuhole.Comment;
import io.github.gsxj2014.pkuhole.PkuHoleBoomException;
import io.github.gsxj2014.pkuhole.Topic;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class TopicWindow extends JFrame {

    TopicWindow(Topic topic) {
        final Container container = getContentPane();
        container.setLayout(new BorderLayout());

        final JTextArea topicText = new JTextArea();
        topicText.setEditable(false);
        topicText.setLineWrap(true);
        topicText.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 16));
        topicText.setText(topic.toFormattedString());
        container.add(topicText, BorderLayout.NORTH);

        final JList<Comment> commentList = new JList<>();
        commentList.setCellRenderer(new CommentCellRenderer());
        commentList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        commentList.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                commentList.setFixedCellHeight(10);
                commentList.setFixedCellHeight(-1);
            }
        });
        final JScrollPane scrollPane = new JScrollPane(commentList);
        container.add(scrollPane, BorderLayout.CENTER);

        try {
            List<Comment> comments = PkuHoleApp.PKU_HOLE.getComments(topic.id);

            commentList.setModel(new AbstractListModel<Comment>() {
                @Override
                public int getSize() {
                    return comments.size();
                }

                @Override
                public Comment getElementAt(int index) {
                    return comments.get(index);
                }
            });

            commentList.setFixedCellHeight(10);
            commentList.setFixedCellHeight(-1);

        } catch (PkuHoleBoomException e) {
            e.printStackTrace();
        }

        setTitle(String.format("Topic #%d", topic.id));
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setMinimumSize(new Dimension(400, 300));
        setPreferredSize(new Dimension(900, 600));
        pack();
        setVisible(true);
    }
}
