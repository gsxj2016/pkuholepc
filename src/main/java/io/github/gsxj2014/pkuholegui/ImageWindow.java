package io.github.gsxj2014.pkuholegui;

import io.github.gsxj2014.pkuhole.PkuHoleBoomException;
import io.github.gsxj2014.pkuhole.Topic;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Image;

@SuppressWarnings("WeakerAccess")
public class ImageWindow extends JFrame {

    ImageWindow(Topic topic) {
        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());

        JLabel label = new JLabel("");
        JScrollPane scrollPane = new JScrollPane(label);
        cp.add(scrollPane, BorderLayout.CENTER);

        try {
            Image image = PkuHoleApp.PKU_HOLE.getImage(topic);
            if (image == null) {
                label.setText("[Failed]");
            } else {
                label.setIcon(new ImageIcon(image));
            }
        } catch (PkuHoleBoomException e) {
            label.setText("[Failed]");
            e.printStackTrace();
        }

        setTitle("Picture: " + topic.url);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setVisible(true);
    }

}
