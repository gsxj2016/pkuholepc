package io.github.gsxj2014.pkuholegui;

import io.github.gsxj2014.pkuhole.Comment;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

public class CommentCellRenderer extends JTextArea implements ListCellRenderer<Comment> {

    private final JPanel panel;

    CommentCellRenderer() {
        panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(this, BorderLayout.CENTER);
        setEditable(false);
        setLineWrap(true);
        setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 16));
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends Comment> list, Comment value, int index, boolean isSelected, boolean cellHasFocus) {
        setText(value.toFormattedString());
        setBackground(isSelected ? Color.CYAN : Color.WHITE);
        return panel;
    }
}
