package io.github.gsxj2014.pkuholegui;

import io.github.gsxj2014.pkuhole.PkuHoleBoomException;
import io.github.gsxj2014.pkuhole.Topic;
import io.github.gsxj2014.pkuhole.TopicType;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class MainWindow extends JFrame {

    private static final String MAIN_WINDOW_TITLE = "PKU Hole Viewer";
    private static final int MAX_PAGE = 99999;

    private int page = 1;
    private final JSpinner spinner;
    private final JList<Topic> topicList;
    private final JTextField searchText;

    MainWindow() {
        final Container container = getContentPane();
        container.setLayout(new BorderLayout());

        final JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        spinner = new JSpinner(new SpinnerNumberModel(1, 1, MAX_PAGE, 1));
        final JButton buttonGo = new JButton("Go");
        final JButton buttonFirst = new JButton("1");
        final JButton buttonPrev = new JButton("<-");
        final JButton buttonNext = new JButton("->");
        searchText = new JTextField("");
        final JButton buttonSearch = new JButton("Search");
        final JButton buttonOne = new JButton("#");

        buttonGo.addActionListener(e -> {
            setPageNumber();
            loadPage();
        });

        buttonFirst.addActionListener(e -> {
            page = 1;
            loadPage();
        });

        buttonNext.addActionListener(e -> {
            ++page;
            if (page > MAX_PAGE) page = MAX_PAGE;
            loadPage();
        });

        buttonPrev.addActionListener(e -> {
            --page;
            if (page < 1) page = 1;
            loadPage();
        });

        buttonSearch.addActionListener(e -> searchTopics());
        buttonOne.addActionListener(e -> getOneTopic());

        panel.add(spinner);
        panel.add(buttonGo);
        panel.add(buttonFirst);
        panel.add(buttonPrev);
        panel.add(buttonNext);
        panel.add(searchText);
        panel.add(buttonSearch);
        panel.add(buttonOne);

        container.add(panel, BorderLayout.NORTH);

        topicList = new JList<>();
        topicList.setCellRenderer(new TopicCellRenderer());
        topicList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        topicList.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                refreshCellSize();
            }
        });
        topicList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    Topic topic = topicList.getSelectedValue();
                    if (topic != null) {
                        new TopicWindow(topic);
                        if (topic.type == TopicType.IMAGE) {
                            new ImageWindow(topic);
                        }
                    }
                }
            }
        });
        final JScrollPane scrollPane = new JScrollPane(topicList);
        container.add(scrollPane, BorderLayout.CENTER);

        setTitle(MAIN_WINDOW_TITLE);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(400, 300));
        setPreferredSize(new Dimension(900, 600));
        loadPage();
        pack();
        setVisible(true);
    }

    private void refreshCellSize() {
        topicList.setFixedCellHeight(10);
        topicList.setFixedCellHeight(-1);
    }

    private void setPageNumber() {
        page = (Integer) spinner.getValue();
        if (page < 1) page = 1;
        if (page > MAX_PAGE) page = MAX_PAGE;
    }

    private void updateTopicList(List<Topic> topics) {
        topicList.setModel(
                new AbstractListModel<Topic>() {
                    @Override
                    public int getSize() {
                        return topics.size();
                    }

                    @Override
                    public Topic getElementAt(int index) {
                        return topics.get(index);
                    }
                }
        );

        refreshCellSize();
    }

    private void loadPage() {
        spinner.setValue(page);
        try {
            final List<Topic> topics = PkuHoleApp.PKU_HOLE.getTopics(page);
            updateTopicList(topics);
        } catch (PkuHoleBoomException e) {
            e.printStackTrace();
        }
    }

    private void searchTopics() {
        try {
            final List<Topic> topics = PkuHoleApp.PKU_HOLE.searchTopics(searchText.getText());
            updateTopicList(topics);
        } catch (PkuHoleBoomException e) {
            e.printStackTrace();
        }
    }

    private void getOneTopic() {
        try {
            final List<Topic> topics = PkuHoleApp.PKU_HOLE.getOneTopic(searchText.getText());
            updateTopicList(topics);
        } catch (PkuHoleBoomException e) {
            e.printStackTrace();
        }
    }

}
